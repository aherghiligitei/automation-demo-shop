package org.fastrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static org.fastrackit.Constants.HOME_PAGE;

public class HomePage {

    private final SelenideElement greetings = $(".navbar-text > span");
    private final SelenideElement logo = $(".navbar-brand .fa-shopping-bag");
    private final String logoHoverText = logo.parent().getAttribute("href");
    private final SelenideElement favoritesIcon = $(".navbar-nav [data-icon=heart]");
    private final SelenideElement cartIcon = $(".shopping-cart-icon [data-icon=shopping-cart] ");

    private final SelenideElement loginIcon = $(".fa-sign-in-alt");
    private final SelenideElement logOutIcon = $(".fa-sign-out-alt");
    private final SelenideElement loginModal = $(".modal-content");

    private final SelenideElement modalCloseButton = $(".close");
    private final SelenideElement resetAppButton = $(".btn-link .fa-undo");

    private final SelenideElement searchButton = $(".col-md-auto [type=button]");
    private final SelenideElement searchField = $("#input-search");
    private final String searchFieldEmpty = searchField.getText();
    private final SelenideElement search = $("#input-search");

    private final SelenideElement sort = $(".sort-products-select");
    private final SelenideElement sortAZ = $(".sort-products-select [value=az]");
    private final SelenideElement sortZA = $(".sort-products-select [value=za]");
    private final SelenideElement sortLoHi = $(".sort-products-select [value=lohi]");
    private final SelenideElement sortHiLo = $(".sort-products-select [value=hilo]");
    private final boolean sortDefaultSelected = sortAZ.isSelected();

    private final SelenideElement navLink = $(".nav-link");
    private final SelenideElement questionIcon = $(".btn-link [data-icon=question]");
    private final String text = resetAppButton.parent().getAttribute("title");

    private final SelenideElement helpModalTitle = $(" .modal-content .text-muted");
    private final SelenideElement close = $(".close");
    private final SelenideElement table = $(" .modal-content .table");
    private final SelenideElement modalOpen = $("#root");


    private final SelenideElement username = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement loginButton = $(".btn-primary");
    private boolean validCredentials;
    public final SelenideElement resetButton = $(".fa-undo");



    public boolean isResetStateButtonDisplayed() {
        return resetButton.exists() && resetButton.isDisplayed();
    }



    /*** Page greetings message*/

    public String greetingsMessage() {
        return greetings.getText();
    }

    public boolean isGreetingsMessageDisplayed() {
        return greetings.exists() && greetings.isDisplayed();
    }

    /*** Page logo*/

    public boolean isLogoDisplayed() {
        return logo.exists() && logo.isDisplayed();

    }
    public String logoLinkOnHover() {
        return logoHoverText;
    }
    public String logoRedirectUrl() {
        return logo.parent().getAttribute("href");}

    /*** Wishlist*/

    public boolean isFavoritesIconDisplayed() {
        return favoritesIcon.exists();
    }

    public String wishListRedirectURL() {
        return favoritesIcon.parent().getAttribute("href");
    }

    /***Cart*/

        public boolean isCartIconDisplayed () {
            return cartIcon.exists();
        }

        public String cartRedirectUrl () {
            return cartIcon.parent().getAttribute("href");
        }

    /***Login Modal*/


    public boolean isLoginIconDisplayed() {
        return loginIcon.exists();
    }

    public void loginClick() {
        loginIcon.click();
    }

    public boolean isLoginModalDisplayed() {
        return loginModal.exists();
    }

    public boolean isLoginModalActive() {
        return loginModal.exists();
    }


    /**Reset application

     */
    public void resetApp() {
        if (modalCloseButton.exists() && modalCloseButton.isDisplayed()) {
           modalClose();
        } else {
            resetAppButton.click();
        }
    }

    public void modalClose() {
        modalCloseButton.click();
    }

/**Search

 */


    public boolean isSearchButtonDisplayed() {
        return searchButton.exists();
    }
    public String getSearchButtonText() {
        return searchButton.getOwnText();
    }
    public boolean isSearchDisplayed() {
        return searchField.exists();
    }
    public Integer isSearchFieldEmpty() {
        return searchFieldEmpty.length();
    }

    public boolean searchActive() {
        return search.isEnabled();
    }

/**Sort*/

    public boolean isSortDisplayed() {
        return sort.exists();
    }

    public boolean verifySortDefaultSelected() {
        return sortDefaultSelected;
    }

    public String sortingAtoZ() {
        return sortAZ.getText();
    }

    public String sortingZtoA() {
        return sortZA.getText();
    }

    public String sortingHighToLow() {
        return sortHiLo.getText();
    }

    public String sortingLowToHigh() {
        return sortLoHi.getText();
    }

    public boolean isDemoShopLinkDisplayed() {
        return navLink.exists() && navLink.isDisplayed();
    }

    public String demoShopMessage() {
        return navLink.getText();
    }

    public String clickedDemoShopLink() {
        return HOME_PAGE;
    }

    public boolean isQuestionIconDisplayed() {
        return questionIcon.isDisplayed();
    }

    public boolean isResetButtonDisplayed() {
        return resetAppButton.isDisplayed() && resetAppButton.exists();
    }

    public String resetAppHoverText() {
        return text;
    }


    /**Help Modal Window*/

    public void open() {
        questionIcon.click();
    }

    public String title() {
        return helpModalTitle.getText();
    }

    public String isActive() {
        return modalOpen.getAttribute("aria-hidden");
    }

    public void close() {
        close.click();
    }

    public boolean status() {
        return helpModalTitle.isDisplayed();
    }

    public boolean isValidUsernamesTableDisplayed() {
        return table.isDisplayed() && table.exists();
    }

    /**Usernames*/

    public String validUsernamesTable() {
        return table.parent().getText();
    }

    public String getLoginStatus() {
        if ((username.equals("dino") || username.equals("beetle") || username.equals("turtle") || username.equals("locked")) && password.equals("choochoo")) {
            validCredentials = true;
        }

        if (validCredentials == true) {
            loginButton.click();
        }
        String errorMessage = "Invalid username or password";
        return errorMessage;
    }

    public void dinoLoggedIn() {
        username.sendKeys("dino");
        password.sendKeys("choochoo");
        loginButton.click();
    }

    public void logOutClick() {
        logOutIcon.click();
    }

    public void beetleLoggedIn() {
        username.sendKeys("beetle");
        password.sendKeys("choochoo");
        loginButton.click();
    }

    public void turtleLoggedIn() {
        username.sendKeys("turtle");
        password.sendKeys("choochoo");
        loginButton.click();
    }

    public void lockedLoggedIn() {
        username.sendKeys("locked");
        password.sendKeys("choochoo");
        loginButton.click();
    }


    }
