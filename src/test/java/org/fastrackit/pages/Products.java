package org.fastrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Products {

    private final SelenideElement AwesomeGraniteChips = $("a[href='#/product/1']");
    private final SelenideElement AwesomeMetalChair = $("a[href='#/product/3']");
    private final SelenideElement AwesomeSoftShirt = $("a[href='#/product/5']");
    private final SelenideElement GorgeousSoftPizza = $("a[href='#/product/9']");
    private final SelenideElement IncredibleConcreteHat = $("a[href='#/product/2']");
    private final SelenideElement LicensedSteelGloves = $("a[href='#/product/8']");
    private final SelenideElement PracticalMetalMouse = $("a[href='#/product/7']");
    private final SelenideElement PracticalWoodenBacon1 = $("a[href='#/product/4']");
    private final SelenideElement PracticalWoodenBacon2 = $("a[href='#/product/6']");
    private final SelenideElement RefinedFrozenMouse = $("a[href='#/product/0']");


    public boolean AwesomeGraniteChipsIsDisplayed() {
        return AwesomeGraniteChips.exists() && AwesomeGraniteChips.isDisplayed();
    }

    public boolean AwesomeMetalChairIsDisplayed() {
        return AwesomeMetalChair.exists() && AwesomeMetalChair.isDisplayed();
    }

    public boolean AwesomeSoftShirtIsDisplayed() {
        return AwesomeSoftShirt.exists() && AwesomeSoftShirt.isDisplayed();
    }

    public boolean GorgeousSoftPizzaIsDisplayed() {
        return GorgeousSoftPizza.exists() && GorgeousSoftPizza.isDisplayed();
    }

    public boolean IncredibleConcreteHatIsDisplayed() {
        return IncredibleConcreteHat.exists() && IncredibleConcreteHat.isDisplayed();
    }

    public boolean LicensedSteelGlovesIsDisplayed() {
        return LicensedSteelGloves.exists() && LicensedSteelGloves.isDisplayed();
    }

    public boolean PracticalMetalMouseIsDisplayed() {
        return PracticalMetalMouse.exists() && PracticalMetalMouse.isDisplayed();
    }

    public boolean PracticalWoodenBacon1IsDisplayed() {
        return PracticalWoodenBacon1.exists() && PracticalWoodenBacon1.isDisplayed();
    }

    public boolean PracticalWoodenBacon2IsDisplayed() {
        return PracticalWoodenBacon2.exists() && PracticalWoodenBacon2.isDisplayed();
    }

    public boolean RefinedFrozenMouseIsDisplayed() {
        return RefinedFrozenMouse.exists() && RefinedFrozenMouse.isDisplayed();
    }


    public String AwesomeGraniteChipsURL() {
        return AwesomeGraniteChips.getAttribute("href");
    }

    public String AwesomeMetalChairURL() {
        return AwesomeMetalChair.getAttribute("href");
    }

    public String AwesomeSoftShirtURL() {
        return AwesomeSoftShirt.getAttribute("href");
    }

    public String GorgeousSoftPizzaURL() {
        return GorgeousSoftPizza.getAttribute("href");
    }

    public String IncredibleConcreteHatURL() {
        return IncredibleConcreteHat.getAttribute("href");
    }

    public String LicensedSteelGlovesURL() {
        return LicensedSteelGloves.getAttribute("href");
    }

    public String PracticalMetalMouseURL() {
        return PracticalMetalMouse.getAttribute("href");
    }

    public String PracticalWoodenBacon1URL() {
        return PracticalWoodenBacon1.getAttribute("href");
    }

    public String PracticalWoodenBacon2URL() {
        return PracticalWoodenBacon2.getAttribute("href");
    }

    public String RefinedFrozenMouseURL() {
        return RefinedFrozenMouse.getAttribute("href");
    }
}
