package org.fastrackit.pages;

import com.codeborne.selenide.Selenide;

public class BasePage {

    public void refresh() {
        Selenide.refresh();
    }
}
