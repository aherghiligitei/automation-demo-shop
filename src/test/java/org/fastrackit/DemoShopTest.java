package org.fastrackit;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.fastrackit.pages.*;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static org.fastrackit.Constants.*;
import static org.testng.Assert.*;

public class DemoShopTest {

    HomePage homePage;
    Product product;

    @BeforeTest
    public void openPage() {
        open(HOME_PAGE);
        homePage = new HomePage();
        product = new Product();
    }


    @AfterTest
    public void cleanUp() {
        homePage.resetApp();
    }


    @Test
    public void verify_greetings_message() {
        assertTrue(homePage.isGreetingsMessageDisplayed(), "Welcome message must be displayed.");
        assertEquals(homePage.greetingsMessage(), HELLO_GUEST_GREETINGS_MESSAGE, "Welcome message is displayed.");
    }


    @Test
    public void verify_homePageLogoUrl() {
        assertEquals(homePage.logoLinkOnHover(), "https://fasttrackit-test.netlify.app/", "Hovering on logo element displays homepage address.");
        assertEquals(homePage.logoRedirectUrl(), HOME_PAGE, "Clicking the Logo, redirects to homepage");
    }

    @Test
    public void verify_wishlistUrl() {
        assertTrue(homePage.isFavoritesIconDisplayed(), "Wishlist icon is displayed");
        assertEquals(homePage.wishListRedirectURL(), HOME_PAGE + "#/wishlist", "Redirects to wishlist page.");
    }

    @Test
    public void verify_cartUrl() {
        assertTrue(homePage.isCartIconDisplayed(), "Cart icon is displayed.");
        assertEquals(homePage.cartRedirectUrl(), HOME_PAGE + "#/cart", "Redirects to cart page.");
    }

    @Test
    public void verify_loginModalWindowOpens() {
        assertTrue(homePage.isLoginModalActive(), "Login Modal Window exists and is open.");
       assertTrue(homePage.isLoginModalActive(), "After clicking X Login Modal Window closes.");


    }

    @Test
    public void verify_searchField() {
        assertTrue(homePage.isSearchDisplayed(), "Search field is displayed.");
        assertEquals(homePage.isSearchFieldEmpty(), 0, "Testing if search field is empty.");
        assertTrue(homePage.searchActive(), "Search option is available after click");
    }

    @Test
    public void verify_searchButton() {
        assertTrue(homePage.isSearchButtonDisplayed(), " Search button is displayed.");
        assertEquals(homePage.getSearchButtonText(), "Search", "Search button text is displayed.");
    }

    @Test
    public void verify_sort_products() {
        assertTrue(homePage.isSortDisplayed(), "Sort options  are displayed");
        assertTrue(homePage.verifySortDefaultSelected(), "Sort by name(A to Z) default selected");
        assertEquals(homePage.sortingZtoA(), "Sort by name (Z to A)", "Sorting the products from Z to A ");
        assertEquals(homePage.sortingLowToHigh(), "Sort by price (low to high)", "Sorting the products from low to high");
        assertEquals(homePage.sortingHighToLow(), "Sort by price (high to low)", "Sorting the products from high to low");
        assertEquals(homePage.sortingAtoZ(), "Sort by name (A to Z)", "Sorting the products from Z to A ");
    }

    @Test
    public void verify_footerDemoShopUrl() {
        assertTrue(homePage.isDemoShopLinkDisplayed(), "DemoShop footer link exists and displayed ");
        assertEquals(homePage.demoShopMessage(), "Demo Shop | build date 2021-05-21 14:04:30 GTBDT", "Footer DemoShop text is displayed.");
        assertEquals(homePage.clickedDemoShopLink(), HOME_PAGE, "Clicking DemoShop footer link redirected to homepage");
    }

    @Test
    public void verify_helModalWindowOpen() {
        homePage.open();
        assertTrue(homePage.isQuestionIconDisplayed(), "Question icon is displayed.");
        assertEquals(homePage.isActive(), "true", "Help Modal Window is active.");
    }



    @Test
    public void verify_helpModalWindowClose() {
        homePage.open();
        assertEquals(homePage.isActive(), "true", "Help Modal Window is active.");
       homePage.close();
        assertTrue(homePage.status(), "Help Modal Window is not open.");
        homePage.close();
    }

    @Test
    public void verify_helpModalWindowContent() {

        homePage.open();
        assertEquals(homePage.title(), "Help", "Help Modal Window title is displayed.");
        assertTrue(homePage.status(), "Help Modal Window content is displayed");
        assertTrue(homePage.isValidUsernamesTableDisplayed(), "Help Modal Window table is displayed");
        assertEquals(homePage.validUsernamesTable(), """
                Valid usernames
                Username Type Password
                dino normal user choochoo
                beetle user with bugs choochoo
                turtle slow user choochoo
                locked locked out user choochoo""", "Details containing Valid usernames on helpModal");
        homePage.close();
    }


    @Test
    public void verify_resetApplicationButton() {
        assertTrue(homePage.isResetButtonDisplayed(), "Reset application button is displayed.");
        assertEquals(homePage.resetAppHoverText(), "Reset the application state", "Reset the application state is displayed.");
    }

    @Test
    public void verify_LoginModalDino() {
        homePage.loginClick();
        assertEquals(homePage.getLoginStatus(), "Invalid username or password", "Check Login state");
        homePage.dinoLoggedIn();
        assertEquals(homePage.greetingsMessage(), "Hi dino!", "Verify login successful with user dino");
        homePage.logOutClick();
    }

    @Test
    public void verify_LoginModalBeetle() {
        homePage.loginClick();
        assertEquals(homePage.getLoginStatus(), "Invalid username or password", "Check Login state");
        homePage.beetleLoggedIn();
        assertEquals(homePage.greetingsMessage(), "Hi beetle!", "Verify login successful with user beetle");
        homePage.logOutClick();
    }

    @Test
    public void verify_LoginModalTurtle() {
        homePage.loginClick();
        assertEquals(homePage.getLoginStatus(), "Invalid username or password", "Check Login state");
        homePage.turtleLoggedIn();
        assertEquals(homePage.greetingsMessage(), "Hi turtle!", "Verify login successful with user turtle");
        homePage.logOutClick();
    }


    @Test
    public void verify_loginIcon() {
        assertTrue(homePage.isLoginIconDisplayed(), "Login icon exist and is displayed.");
    }


    @Test
    public void verify_AwesomeGraniteChips() {
        assertTrue(product.AwesomeGraniteChipsIsDisplayed(), "Product ID #1 is displayed.");
        assertEquals(product.AwesomeGraniteChipsURL(), HOME_PAGE + "#/product/1", "Product ID #1 page is opened");
    }

    @Test
    public void verify_AwesomeMetalChair() {
        assertTrue(product.AwesomeMetalChairIsDisplayed(), "Product ID #3 is displayed.");
        assertEquals(product.AwesomeMetalChairURL(), HOME_PAGE + "#/product/3", "Product ID #3 page is opened.");
    }

    @Test
    public void verify_AwesomeSoftShirt() {
        assertTrue(product.AwesomeSoftShirtIsDisplayed(), "Product ID #5 is displayed.");
        assertEquals(product.AwesomeSoftShirtURL(), HOME_PAGE + "#/product/5", "Product ID #5 page is opened.");
    }

    @Test
    public void verify_GorgeousSoftPizza() {
        assertTrue(product.GorgeousSoftPizzaIsDisplayed(), "Product ID #9 is displayed.");
        assertEquals(product.GorgeousSoftPizzaURL(), HOME_PAGE + "#/product/9", "Product ID #9 is opened");
    }

    @Test
    public void verify_IncredibleConcreteHat() {
        assertTrue(product.IncredibleConcreteHatIsDisplayed(), "Product ID #2 is displayed.");
        assertEquals(product.IncredibleConcreteHatURL(), HOME_PAGE + "#/product/2", "Product ID #2 is opened.");
    }

    @Test
    public void verify_LicensedSteelGloves() {
        assertTrue(product.LicensedSteelGlovesIsDisplayed(), "Product ID #8 is displayed.");
        assertEquals(product.LicensedSteelGlovesURL(), HOME_PAGE + "#/product/8", "Product ID #8 is opened.");
    }

    @Test
    public void verify_PracticalMetalMouse() {
        assertTrue(product.PracticalMetalMouseIsDisplayed(), "Product ID #7 is displayed.");
        assertEquals(product.PracticalMetalMouseURL(), HOME_PAGE + "#/product/7", "Product ID #7 is opened.");
    }

    @Test
    public void verify_PracticalWoodenBacon1() {
        assertTrue(product.PracticalWoodenBacon1IsDisplayed(), "Product ID #4 is displayed.");
        assertEquals(product.PracticalWoodenBacon1URL(), HOME_PAGE + "#/product/4", "Product ID #4 is opened.");
    }

    @Test
    public void verify_PracticalWoodenBacon2() {
        assertTrue(product.PracticalWoodenBacon2IsDisplayed(), "Product ID #6 is displayed.");
        assertEquals(product.PracticalWoodenBacon2URL(), HOME_PAGE + "#/product/6", "Product ID #6 is opened.");
    }

    @Test
    public void verify_RefinedFrozenMouse() {
        assertTrue(product.RefinedFrozenMouseIsDisplayed(), "Product ID #0 is displayed.");
        assertEquals(product.RefinedFrozenMouseURL(), HOME_PAGE + "#/product/0", "Product ID #0 is opened.");
    }




}
