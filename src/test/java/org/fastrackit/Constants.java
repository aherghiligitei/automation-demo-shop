package org.fastrackit;

public class Constants {

    public static final String HOME_PAGE = "https://fasttrackit-test.netlify.app/";

    public static final String CART_PAGE = "https://fasttrackit-test.netlify.app/#/cart";

    public static final String WISHLIST_PAGE = "https://fasttrackit-test.netlify.app/#/wishlist";

    public static final String HELLO_GUEST_GREETINGS_MESSAGE = "Hello guest!";

}
